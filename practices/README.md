# Exercises (misc)

## What is it obvious that I value

* May I interview you
* What is it obvious to you that I value?
* May I tell you what it is obvious that you value?
* What was that like?
* What I now see as available, or possible, or what new future it gives you?
